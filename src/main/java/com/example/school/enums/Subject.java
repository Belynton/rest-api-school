package com.example.school.enums;

public enum Subject {
    MATH,
    BIOLOGY,
    CHEMISTRY,
    PHYSICS,
    GEOGRAPHY,
    RUSSIAN_LANGUAGE,
    ENGLISH_LANGUAGE
}
