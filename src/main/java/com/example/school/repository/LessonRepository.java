package com.example.school.repository;

import com.example.school.entity.Lesson;
import com.example.school.enums.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface LessonRepository extends JpaRepository<Lesson, UUID> {

    List<Lesson> getLessonsBySubject(Subject subject);

    List<Lesson> getLessonsByStudentClass_Id(UUID id);

    @Query(value = "SELECT * FROM lesson WHERE cast(start_date AS DATE) = ?1", nativeQuery = true)
    List<Lesson> getLessonsByStartDateTime(LocalDate date);
}
