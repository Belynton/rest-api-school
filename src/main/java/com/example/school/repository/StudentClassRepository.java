package com.example.school.repository;

import com.example.school.entity.StudentClass;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface StudentClassRepository extends JpaRepository<StudentClass, UUID> {
}
