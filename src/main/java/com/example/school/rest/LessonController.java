package com.example.school.rest;

import com.example.school.enums.Subject;
import com.example.school.rest.api.LessonApi;
import com.example.school.rest.dto.LessonRqDto;
import com.example.school.rest.dto.LessonRsDto;
import com.example.school.service.LessonService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class LessonController implements LessonApi {

    private LessonService lessonService;

    @Override
    public LessonRsDto create(LessonRqDto lessonRqDto) {
        return lessonService.create(lessonRqDto);
    }

    @Override
    public LessonRsDto getById(UUID id) {
        return lessonService.getById(id);
    }

    @Override
    public List<LessonRsDto> getAll() {
        return lessonService.getAll();
    }

    @Override
    public List<LessonRsDto> getAllBySubject(Subject subject) {
        return lessonService.getAllBySubject(subject);
    }

    @Override
    public List<LessonRsDto> getAllByStudentClass(UUID studentClassId) {
        return lessonService.getAllByStudentClass(studentClassId);
    }

    @Override
    public List<LessonRsDto> getAllByDate(LocalDate date) {
        return lessonService.getAllByDate(date);
    }


    @Override
    public LessonRsDto update(UUID id, LessonRqDto lessonRqDto) {
        return lessonService.update(id, lessonRqDto);
    }

    @Override
    public void delete(UUID id) {
        lessonService.delete(id);
    }
}
