package com.example.school.rest;

import com.example.school.rest.api.TeacherApi;
import com.example.school.rest.dto.TeacherRqDto;
import com.example.school.rest.dto.TeacherRsDto;
import com.example.school.service.TeacherService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class TeacherController implements TeacherApi {

    private TeacherService teacherService;

    @Override
    public TeacherRsDto create(TeacherRqDto teacherRqDto) {
        return teacherService.create(teacherRqDto);
    }

    @Override
    public TeacherRsDto getById(UUID id) {
        return teacherService.getById(id);
    }

    @Override
    public List<TeacherRsDto> getAll() {
        return teacherService.getAll();
    }

    @Override
    public TeacherRsDto update(UUID id, TeacherRqDto teacherRqDto) {
        return teacherService.update(id, teacherRqDto);
    }

    @Override
    public void delete(UUID id) {
        teacherService.delete(id);
    }
}
