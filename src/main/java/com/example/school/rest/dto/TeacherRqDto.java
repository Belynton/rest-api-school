package com.example.school.rest.dto;

import com.example.school.enums.Subject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeacherRqDto {

    private String fio;
    private List<Subject> subject;
}
