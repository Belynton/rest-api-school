package com.example.school.rest.dto;

import com.example.school.enums.Subject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeacherRsDto {

    private UUID id;
    private String fio;
    private List<Subject> subject;
}
