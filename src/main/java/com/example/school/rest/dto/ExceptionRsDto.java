package com.example.school.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class ExceptionRsDto {

    private LocalDateTime timeStamp;
    private String message;
}
