package com.example.school.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentClassRsDto {

    private UUID id;
    private String name;
    private List<StudentRsDto> students;
}
