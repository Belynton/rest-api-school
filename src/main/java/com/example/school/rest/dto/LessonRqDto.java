package com.example.school.rest.dto;

import com.example.school.enums.Subject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonRqDto {

    private LocalDateTime startDateTime;
    private Subject subject;
    private UUID teacher;
    private UUID studentClass;
}
