package com.example.school.rest.api;

import com.example.school.enums.Subject;
import com.example.school.rest.dto.ExceptionRsDto;
import com.example.school.rest.dto.LessonRqDto;
import com.example.school.rest.dto.LessonRsDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@RequestMapping(path = "/api/school/lesson")
public interface LessonApi {

    @Operation(summary = "Create lesson", tags = "Lesson", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Create new lesson",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = LessonRsDto.class)))
                    })
    })
    @PostMapping
    LessonRsDto create(@RequestBody LessonRqDto lessonRqDto);

    @Operation(summary = "Get lesson", tags = "Lesson", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Get lesson by id",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = LessonRsDto.class)))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Lesson not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ExceptionRsDto.class)))
                    })
    })
    @GetMapping(path = "/{id}")
    LessonRsDto getById(@PathVariable UUID id);

    @Operation(summary = "Get lessons", tags = "Lesson", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Get all lessons",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = LessonRsDto.class)))
                    })
    })
    @GetMapping
    List<LessonRsDto> getAll();

    @Operation(summary = "Get lessons", tags = "Lesson", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Get all lessons by subject",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = LessonRsDto.class)))
                    })
    })
    @GetMapping(path = {"/get-by-subject/{subject}"})
    List<LessonRsDto> getAllBySubject(@PathVariable Subject subject);

    @Operation(summary = "Get lessons", tags = "Lesson", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Get all lessons by class",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = LessonRsDto.class)))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Class not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ExceptionRsDto.class)))
                    })
    })
    @GetMapping(path = {"/get-by-class/{studentClassId}"})
    List<LessonRsDto> getAllByStudentClass(@PathVariable UUID studentClassId);

    @Operation(summary = "Get lessons", tags = "Lesson", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Get all lessons by date",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = LessonRsDto.class)))
                    })
    })
    @GetMapping(path = {"/get-by-date/{date}"})
    List<LessonRsDto> getAllByDate(@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date);

    @Operation(summary = "Update lesson", tags = "Lesson", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Update lessons data by id",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = LessonRsDto.class)))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Lesson not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ExceptionRsDto.class)))
                    })
    })
    @PostMapping(path = "/{id}")
    LessonRsDto update(@PathVariable UUID id, @RequestBody LessonRqDto lessonRqDto);

    @Operation(summary = "Delete lesson", tags = "Lesson", method = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "202",
                    description = "Delete lesson by id"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Lesson not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ExceptionRsDto.class)))
                    })
    })
    @DeleteMapping(path = "/{id}")
    void delete(@PathVariable UUID id);
}
