package com.example.school.rest.api;

import com.example.school.rest.dto.ExceptionRsDto;
import com.example.school.rest.dto.TeacherRqDto;
import com.example.school.rest.dto.TeacherRsDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequestMapping(path = "/api/school/teacher")
public interface TeacherApi {

    @Operation(summary = "Create teacher", tags = "Teacher", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Create new teacher",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = TeacherRsDto.class)))
                    })
    })
    @PostMapping
    TeacherRsDto create(@RequestBody TeacherRqDto teacherRqDto);

    @Operation(summary = "Get teacher", tags = "Teacher", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Get teacher by id",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = TeacherRsDto.class)))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Teacher not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ExceptionRsDto.class)))
                    })
    })
    @GetMapping(path = "/{id}")
    TeacherRsDto getById(@PathVariable UUID id);

    @Operation(summary = "Get teachers", tags = "Teacher", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Get all teachers",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = TeacherRsDto.class)))
                    })
    })
    @GetMapping
    List<TeacherRsDto> getAll();

    @Operation(summary = "Update teacher", tags = "Teacher", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Update teachers data by id",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = TeacherRsDto.class)))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Teacher not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ExceptionRsDto.class)))
                    })
    })
    @PostMapping(path = "/{id}")
    TeacherRsDto update(@PathVariable UUID id, @RequestBody TeacherRqDto teacherRqDto);

    @Operation(summary = "Delete teacher", tags = "Teacher", method = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "202",
                    description = "Delete teacher by id"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Teacher not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ExceptionRsDto.class)))
                    })
    })
    @DeleteMapping(path = "/{id}")
    void delete(@PathVariable UUID id);
}
