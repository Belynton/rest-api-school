package com.example.school.rest.api;

import com.example.school.rest.dto.ExceptionRsDto;
import com.example.school.rest.dto.StudentClassRqDto;
import com.example.school.rest.dto.StudentClassRsDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequestMapping(path = "/api/school/student-class")
public interface StudentClassApi {

    @Operation(summary = "Create class", tags = "Class", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Create new class",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = StudentClassRsDto.class)))
                    })
    })
    @PostMapping
    StudentClassRsDto create(@RequestBody StudentClassRqDto studentClassRqDto);

    @Operation(summary = "Get class", tags = "Class", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Get class by id",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = StudentClassRsDto.class)))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Class not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ExceptionRsDto.class)))
                    })
    })
    @GetMapping(path = "/{id}")
    StudentClassRsDto getById(@PathVariable UUID id);

    @Operation(summary = "Get classes", tags = "Class", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Get all classes",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = StudentClassRsDto.class)))
                    })
    })
    @GetMapping
    List<StudentClassRsDto> getAll();

    @Operation(summary = "Update class", tags = "Class", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Update class data by id",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = StudentClassRsDto.class)))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Class not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ExceptionRsDto.class)))
                    })
    })
    @PostMapping(path = "/{id}")
    StudentClassRsDto update(@PathVariable UUID id, @RequestBody StudentClassRqDto studentClassRqDto);

    @Operation(summary = "Delete class", tags = "Class", method = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "202",
                    description = "Delete class by id"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Class not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ExceptionRsDto.class)))
                    })
    })
    @DeleteMapping(path = "/{id}")
    void delete(@PathVariable UUID id);
}
