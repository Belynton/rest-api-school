package com.example.school.rest.api;

import com.example.school.rest.dto.ExceptionRsDto;
import com.example.school.rest.dto.StudentRqDto;
import com.example.school.rest.dto.StudentRsDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequestMapping(path = "/api/school/student")
public interface StudentApi {

    @Operation(summary = "Create student", tags = "Student", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Create new student",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = StudentRsDto.class)))
                    })
    })
    @PostMapping
    StudentRsDto create(@RequestBody StudentRqDto studentRqDto);

    @Operation(summary = "Get student", tags = "Student", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Get student by id",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = StudentRsDto.class)))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Student not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ExceptionRsDto.class)))
                    })
    })
    @GetMapping(path = "/{id}")
    StudentRsDto getById(@PathVariable UUID id);

    @Operation(summary = "Get students", tags = "Student", method = "GET")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Get all students",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = StudentRsDto.class)))
                    })
    })
    @GetMapping
    List<StudentRsDto> getAll();

    @Operation(summary = "Update student", tags = "Student", method = "POST")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Update students data by id",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = StudentRsDto.class)))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Student not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ExceptionRsDto.class)))
                    })
    })
    @PostMapping(path = "/{id}")
    StudentRsDto update(@PathVariable UUID id, @RequestBody StudentRqDto studentRqDto);

    @Operation(summary = "Delete student", tags = "Student", method = "DELETE")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "202",
                    description = "Delete student by id"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Student not found",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ExceptionRsDto.class)))
                    })
    })
    @DeleteMapping(path = "/{id}")
    void delete(@PathVariable UUID id);
}
