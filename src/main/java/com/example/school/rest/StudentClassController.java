package com.example.school.rest;

import com.example.school.rest.api.StudentClassApi;
import com.example.school.rest.dto.StudentClassRqDto;
import com.example.school.rest.dto.StudentClassRsDto;
import com.example.school.service.StudentClassService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class StudentClassController implements StudentClassApi {

    private StudentClassService studentClassService;

    @Override
    public StudentClassRsDto create(StudentClassRqDto studentClassRqDto) {
        return studentClassService.create(studentClassRqDto);
    }

    @Override
    public StudentClassRsDto getById(UUID id) {
        return studentClassService.getById(id);
    }

    @Override
    public List<StudentClassRsDto> getAll() {
        return studentClassService.getAll();
    }

    @Override
    public StudentClassRsDto update(UUID id, StudentClassRqDto studentClassRqDto) {
        return studentClassService.update(id, studentClassRqDto);
    }

    @Override
    public void delete(UUID id) {
        studentClassService.delete(id);
    }
}
