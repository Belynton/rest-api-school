package com.example.school.rest;

import com.example.school.rest.api.StudentApi;
import com.example.school.rest.dto.StudentRqDto;
import com.example.school.rest.dto.StudentRsDto;
import com.example.school.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class StudentController implements StudentApi {

    private StudentService studentService;

    @Override
    public StudentRsDto create(StudentRqDto studentRqDto) {
        return studentService.create(studentRqDto);
    }

    @Override
    public StudentRsDto getById(UUID id) {
        return studentService.getById(id);
    }

    @Override
    public List<StudentRsDto> getAll() {
        return studentService.getAll();
    }

    @Override
    public StudentRsDto update(UUID id, StudentRqDto studentRqDto) {
        return studentService.update(id, studentRqDto);
    }

    @Override
    public void delete(UUID id) {
        studentService.delete(id);
    }
}
