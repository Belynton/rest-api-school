package com.example.school.rest.extension;

import com.example.school.rest.dto.ExceptionRsDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class ExceptionHandlingController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity<ExceptionRsDto> handleDataNotFoundException(Exception ex) {
        return new ResponseEntity<>(
                new ExceptionRsDto(LocalDateTime.now(), ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NotSupportedSubjectException.class)
    public ResponseEntity<ExceptionRsDto> handleNotSupportedSubjectException(Exception ex) {
        return new ResponseEntity<>(
                new ExceptionRsDto(LocalDateTime.now(), ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.CONFLICT);
    }
}
