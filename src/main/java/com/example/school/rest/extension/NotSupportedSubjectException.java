package com.example.school.rest.extension;

public class NotSupportedSubjectException extends RuntimeException {

    public NotSupportedSubjectException(String message) {
        super(message);
    }
}
