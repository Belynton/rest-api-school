package com.example.school.entity;

import com.example.school.enums.Subject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "lesson")
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id = UUID.randomUUID();
    @Column(name = "start_date")
    private LocalDateTime startDateTime;
    @Enumerated(EnumType.STRING)
    private Subject subject;
    @OneToOne
    private Teacher teacher;
    @OneToOne
    private StudentClass studentClass;
}
