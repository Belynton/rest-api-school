package com.example.school.service;

import com.example.school.enums.Subject;
import com.example.school.rest.dto.LessonRqDto;
import com.example.school.rest.dto.LessonRsDto;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface LessonService {

    LessonRsDto create(LessonRqDto lessonRqDto);

    LessonRsDto getById(UUID id);

    List<LessonRsDto> getAll();

    List<LessonRsDto> getAllByDate(LocalDate date);

    List<LessonRsDto> getAllBySubject(Subject subject);

    List<LessonRsDto> getAllByStudentClass(UUID studentClassId);

    LessonRsDto update(UUID id, LessonRqDto lessonRqDto);

    void delete(UUID id);
}
