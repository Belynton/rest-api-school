package com.example.school.service.impl;

import com.example.school.entity.Lesson;
import com.example.school.entity.StudentClass;
import com.example.school.entity.Teacher;
import com.example.school.enums.Subject;
import com.example.school.mapper.LessonMapper;
import com.example.school.repository.LessonRepository;
import com.example.school.repository.StudentClassRepository;
import com.example.school.repository.TeacherRepository;
import com.example.school.rest.dto.LessonRqDto;
import com.example.school.rest.dto.LessonRsDto;
import com.example.school.rest.extension.DataNotFoundException;
import com.example.school.rest.extension.NotSupportedSubjectException;
import com.example.school.service.LessonService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class LessonServiceImpl implements LessonService {

    private final String LESSON_NOT_FOUND_MSG = "Lesson with id = %s not found!";
    private final String TEACHER_NOT_FOUND_MSG = "Teacher with id = %s not found!";
    private final String CLASS_NOT_FOUND_MSG = "Class with id = %s not found!";
    private final String NOT_SUPPORTED_SUBJECT_MSG = "Subject %s can't be applied to this teacher!";

    private LessonMapper lessonMapper;
    private LessonRepository lessonRepository;
    private TeacherRepository teacherRepository;
    private StudentClassRepository studentClassRepository;

    @Override
    @Transactional
    public LessonRsDto create(LessonRqDto lessonRqDto) {
        var teacher = findTeacherById(lessonRqDto.getTeacher());
        throwIfTeacherNotSupportSubject(teacher, lessonRqDto);
        var lesson = lessonMapper.requestToEntity(lessonRqDto);
        lesson.setTeacher(teacher);
        lesson.setStudentClass(findStudentClassById(lessonRqDto.getStudentClass()));
        return lessonMapper.entityToResponse(lessonRepository.save(lesson));
    }

    @Override
    public LessonRsDto getById(UUID id) {
        return lessonMapper.entityToResponse(findLessonById(id));
    }

    @Override
    public List<LessonRsDto> getAll() {
        return lessonRepository.findAll().stream().map(lessonMapper::entityToResponse).toList();
    }

    @Override
    public List<LessonRsDto> getAllByDate(LocalDate date) {
        return lessonRepository.getLessonsByStartDateTime(date).stream()
                .map(lessonMapper::entityToResponse).toList();
    }

    @Override
    public List<LessonRsDto> getAllBySubject(Subject subject) {
        return lessonRepository.getLessonsBySubject(subject).stream().map(lessonMapper::entityToResponse).toList();
    }

    @Override
    public List<LessonRsDto> getAllByStudentClass(UUID studentClassId) {
        return lessonRepository.getLessonsByStudentClass_Id(studentClassId).stream()
                .map(lessonMapper::entityToResponse).toList();
    }

    @Override
    @Transactional
    public LessonRsDto update(UUID id, LessonRqDto lessonRqDto) {
        var teacher = findTeacherById(lessonRqDto.getTeacher());
        throwIfTeacherNotSupportSubject(teacher, lessonRqDto);
        var lesson = lessonMapper.requestToEntity(lessonRqDto);
        lesson.setId(findLessonById(id).getId());
        lesson.setTeacher(teacher);
        lesson.setStudentClass(findStudentClassById(lessonRqDto.getStudentClass()));
        return lessonMapper.entityToResponse(lessonRepository.save(lesson));
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        lessonRepository.delete(findLessonById(id));
    }

    private Lesson findLessonById(UUID id) {
        return lessonRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(String.format(LESSON_NOT_FOUND_MSG, id)));
    }

    private Teacher findTeacherById(UUID id) {
        return teacherRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(String.format(TEACHER_NOT_FOUND_MSG, id)));
    }

    private StudentClass findStudentClassById(UUID id) {
        return studentClassRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(String.format(CLASS_NOT_FOUND_MSG, id)));
    }

    private void throwIfTeacherNotSupportSubject(Teacher teacher, LessonRqDto lessonRqDto) {
        if (!teacher.getSubject().contains(lessonRqDto.getSubject())) {
            throw new NotSupportedSubjectException(String.format(NOT_SUPPORTED_SUBJECT_MSG, lessonRqDto.getSubject()));
        }
    }
}
