package com.example.school.service.impl;

import com.example.school.entity.StudentClass;
import com.example.school.mapper.StudentClassMapper;
import com.example.school.repository.StudentClassRepository;
import com.example.school.rest.dto.StudentClassRqDto;
import com.example.school.rest.dto.StudentClassRsDto;
import com.example.school.rest.extension.DataNotFoundException;
import com.example.school.service.StudentClassService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class StudentClassServiceImpl implements StudentClassService {

    private final String CLASS_NOT_FOUND_MSG = "Class with id = %s not found!";

    private StudentClassMapper studentClassMapper;
    private StudentClassRepository studentClassRepository;

    @Override
    @Transactional
    public StudentClassRsDto create(StudentClassRqDto studentClassRqDto) {
        var studentClass = studentClassRepository.save(studentClassMapper.requestToEntity(studentClassRqDto));
        return studentClassMapper.entityToResponse(studentClass);
    }

    @Override
    public StudentClassRsDto getById(UUID id) {
        return studentClassMapper.entityToResponse(findStudentClassById(id));
    }

    @Override
    public List<StudentClassRsDto> getAll() {
        return studentClassRepository.findAll().stream().map(studentClassMapper::entityToResponse).toList();
    }

    @Override
    @Transactional
    public StudentClassRsDto update(UUID id, StudentClassRqDto studentClassRqDto) {
        var studentClass = studentClassMapper.requestToEntity(studentClassRqDto);
        studentClass.setId(findStudentClassById(id).getId());
        return studentClassMapper.entityToResponse(studentClassRepository.save(studentClass));
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        studentClassRepository.delete(findStudentClassById(id));
    }

    private StudentClass findStudentClassById(UUID id) {
        return studentClassRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(String.format(CLASS_NOT_FOUND_MSG, id)));
    }
}
