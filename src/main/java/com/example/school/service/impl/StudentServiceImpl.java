package com.example.school.service.impl;

import com.example.school.entity.Student;
import com.example.school.entity.StudentClass;
import com.example.school.mapper.StudentMapper;
import com.example.school.repository.StudentClassRepository;
import com.example.school.repository.StudentRepository;
import com.example.school.rest.dto.StudentRqDto;
import com.example.school.rest.dto.StudentRsDto;
import com.example.school.rest.extension.DataNotFoundException;
import com.example.school.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final String STUDENT_NOT_FOUND_MSG = "Student with id = %s not found!";
    private final String CLASS_NOT_FOUND_MSG = "Class with id = %s not found!";

    private StudentMapper studentMapper;
    private StudentRepository studentRepository;
    private StudentClassRepository studentClassRepository;

    @Override
    @Transactional
    public StudentRsDto create(StudentRqDto studentRqDto) {
        var student = studentMapper.requestToEntity(studentRqDto);
        student.setStudentClass(findStudentClassById(studentRqDto.getStudentClass()));
        return studentMapper.entityToResponse(studentRepository.save(student));
    }

    @Override
    public StudentRsDto getById(UUID id) {
        return studentMapper.entityToResponse(findStudentById(id));
    }

    @Override
    public List<StudentRsDto> getAll() {
        return studentRepository.findAll().stream().map(studentMapper::entityToResponse).toList();
    }

    @Override
    @Transactional
    public StudentRsDto update(UUID id, StudentRqDto studentRqDto) {
        var student = studentMapper.requestToEntity(studentRqDto);
        student.setId(findStudentById(id).getId());
        student.setStudentClass(findStudentClassById(studentRqDto.getStudentClass()));
        return studentMapper.entityToResponse(studentRepository.save(student));
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        studentRepository.delete(findStudentById(id));
    }

    private Student findStudentById(UUID id) {
        return studentRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(String.format(STUDENT_NOT_FOUND_MSG, id)));
    }

    private StudentClass findStudentClassById(UUID id) {
        return studentClassRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(String.format(CLASS_NOT_FOUND_MSG, id)));
    }
}
