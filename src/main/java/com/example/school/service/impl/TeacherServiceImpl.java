package com.example.school.service.impl;

import com.example.school.entity.Teacher;
import com.example.school.mapper.TeacherMapper;
import com.example.school.repository.TeacherRepository;
import com.example.school.rest.dto.TeacherRqDto;
import com.example.school.rest.dto.TeacherRsDto;
import com.example.school.rest.extension.DataNotFoundException;
import com.example.school.service.TeacherService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class TeacherServiceImpl implements TeacherService {

    private final String TEACHER_NOT_FOUND_MSG = "Teacher with id = %s not found!";

    private TeacherMapper teacherMapper;
    private TeacherRepository teacherRepository;

    @Override
    @Transactional
    public TeacherRsDto create(TeacherRqDto teacherRqDto) {
        var teacher = teacherRepository.save(teacherMapper.requestToEntity(teacherRqDto));
        return teacherMapper.entityToResponse(teacher);
    }

    @Override
    public TeacherRsDto getById(UUID id) {
        return teacherMapper.entityToResponse(findTeacherById(id));
    }

    @Override
    public List<TeacherRsDto> getAll() {
        return teacherRepository.findAll().stream().map(teacherMapper::entityToResponse).toList();
    }

    @Override
    @Transactional
    public TeacherRsDto update(UUID id, TeacherRqDto teacherRqDto) {
        var teacher = teacherMapper.requestToEntity(teacherRqDto);
        teacher.setId(findTeacherById(id).getId());
        return teacherMapper.entityToResponse(teacherRepository.save(teacher));
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        teacherRepository.delete(findTeacherById(id));
    }

    private Teacher findTeacherById(UUID id) {
        return teacherRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(String.format(TEACHER_NOT_FOUND_MSG, id)));
    }
}
