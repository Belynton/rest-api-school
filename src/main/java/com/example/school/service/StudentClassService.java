package com.example.school.service;

import com.example.school.rest.dto.StudentClassRqDto;
import com.example.school.rest.dto.StudentClassRsDto;

import java.util.List;
import java.util.UUID;

public interface StudentClassService {

    StudentClassRsDto create(StudentClassRqDto studentClassRqDto);

    StudentClassRsDto getById(UUID id);

    List<StudentClassRsDto> getAll();

    StudentClassRsDto update(UUID id, StudentClassRqDto studentClassRqDto);

    void delete(UUID id);
}
