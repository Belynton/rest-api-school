package com.example.school.service;

import com.example.school.rest.dto.StudentRqDto;
import com.example.school.rest.dto.StudentRsDto;

import java.util.List;
import java.util.UUID;

public interface StudentService {

    StudentRsDto create(StudentRqDto studentRqDto);

    StudentRsDto getById(UUID id);

    List<StudentRsDto> getAll();

    StudentRsDto update(UUID id, StudentRqDto studentRqDto);

    void delete(UUID id);
}
