package com.example.school.service;

import com.example.school.rest.dto.TeacherRqDto;
import com.example.school.rest.dto.TeacherRsDto;

import java.util.List;
import java.util.UUID;

public interface TeacherService {

    TeacherRsDto create(TeacherRqDto teacherRqDto);

    TeacherRsDto getById(UUID id);

    List<TeacherRsDto> getAll();

    TeacherRsDto update(UUID id, TeacherRqDto teacherRqDto);

    void delete(UUID id);
}
