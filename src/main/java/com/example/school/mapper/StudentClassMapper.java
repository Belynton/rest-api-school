package com.example.school.mapper;

import com.example.school.entity.StudentClass;
import com.example.school.rest.dto.StudentClassRqDto;
import com.example.school.rest.dto.StudentClassRsDto;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@AllArgsConstructor
public class StudentClassMapper {

    private ModelMapper mapper;

    @PostConstruct
    private void init() {
        mapper.createTypeMap(StudentClassRqDto.class, StudentClass.class);
        mapper.createTypeMap(StudentClass.class, StudentClassRsDto.class);
    }

    public StudentClass requestToEntity(StudentClassRqDto studentClassRqDto) {
        return mapper.map(studentClassRqDto, StudentClass.class);
    }

    public StudentClassRsDto entityToResponse(StudentClass studentClass) {
        return mapper.map(studentClass, StudentClassRsDto.class);
    }
}
