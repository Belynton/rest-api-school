package com.example.school.mapper;

import com.example.school.entity.Student;
import com.example.school.rest.dto.StudentRqDto;
import com.example.school.rest.dto.StudentRsDto;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@AllArgsConstructor
public class StudentMapper {

    private ModelMapper mapper;

    @PostConstruct
    private void init() {
        mapper.createTypeMap(StudentRqDto.class, Student.class);
        mapper.createTypeMap(Student.class, StudentRsDto.class)
                .addMapping(student -> student.getStudentClass().getName(), StudentRsDto::setStudentClass);
    }

    public Student requestToEntity(StudentRqDto studentRqDto) {
        return mapper.map(studentRqDto, Student.class);
    }

    public StudentRsDto entityToResponse(Student student) {
        return mapper.map(student, StudentRsDto.class);
    }
}
