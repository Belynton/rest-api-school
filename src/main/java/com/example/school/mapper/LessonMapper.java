package com.example.school.mapper;

import com.example.school.entity.Lesson;
import com.example.school.rest.dto.LessonRqDto;
import com.example.school.rest.dto.LessonRsDto;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@AllArgsConstructor
public class LessonMapper {

    private ModelMapper mapper;

    @PostConstruct
    private void init() {
        mapper.createTypeMap(LessonRqDto.class, Lesson.class);
        mapper.createTypeMap(Lesson.class, LessonRsDto.class);
    }

    public Lesson requestToEntity(LessonRqDto lessonRqDto) {
        return mapper.map(lessonRqDto, Lesson.class);
    }

    public LessonRsDto entityToResponse(Lesson lesson) {
        return mapper.map(lesson, LessonRsDto.class);
    }
}
