package com.example.school.mapper;

import com.example.school.entity.Teacher;
import com.example.school.rest.dto.TeacherRqDto;
import com.example.school.rest.dto.TeacherRsDto;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@AllArgsConstructor
public class TeacherMapper {

    private ModelMapper mapper;

    @PostConstruct
    private void init() {
        mapper.createTypeMap(TeacherRqDto.class, Teacher.class);
        mapper.createTypeMap(Teacher.class, TeacherRsDto.class);
    }

    public Teacher requestToEntity(TeacherRqDto teacherRqDto) {
        return mapper.map(teacherRqDto, Teacher.class);
    }

    public TeacherRsDto entityToResponse(Teacher teacher) {
        return mapper.map(teacher, TeacherRsDto.class);
    }
}
