package com.example.school.repository;

import com.example.school.entity.Lesson;
import com.example.school.entity.StudentClass;
import com.example.school.entity.Teacher;
import com.example.school.enums.Subject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
class LessonRepositoryTest {

    @Autowired
    private LessonRepository lessonRepository;
    @Autowired
    private TestEntityManager entityManager;

    private LocalDateTime dateTime;
    private Subject subject;

    @BeforeEach
    void setupRepository() {
        dateTime = LocalDateTime.now();
        subject = Subject.MATH;
        var teacher = entityManager.persist(new Teacher(null, "teacher", List.of(subject)));
        var studentClass = entityManager.persist(new StudentClass(null, "class", Collections.emptyList()));
        entityManager.persist(new Lesson(null, dateTime, subject, teacher, studentClass));
    }

    @Test
    void should_contains_lessons_with_entered_subject() {
        var returnedLessons = lessonRepository.getLessonsBySubject(subject);
        assertTrue(returnedLessons.size() > 0);
        assertTrue(returnedLessons.stream().map(Lesson::getSubject)
                .allMatch(subjectInList -> subjectInList.equals(subject)));
    }


    @Test
    void should_contains_lessons_with_entered_startDateTime() {
        var returnedLessons = lessonRepository.getLessonsByStartDateTime(dateTime.toLocalDate());
        assertTrue(returnedLessons.size() > 0);
        assertTrue(returnedLessons.stream().map(Lesson::getStartDateTime)
                .allMatch(classesInList -> classesInList.equals(dateTime)));
    }
}