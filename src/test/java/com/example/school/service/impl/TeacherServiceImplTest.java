package com.example.school.service.impl;

import com.example.school.entity.Teacher;
import com.example.school.enums.Subject;
import com.example.school.mapper.TeacherMapper;
import com.example.school.repository.TeacherRepository;
import com.example.school.rest.dto.TeacherRqDto;
import com.example.school.rest.dto.TeacherRsDto;
import com.example.school.rest.extension.DataNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TeacherServiceImplTest {

    private final UUID teacherId = UUID.randomUUID();

    private TeacherRepository teacherRepository;
    private TeacherServiceImpl teacherService;

    @BeforeEach
    void setupService() {
        teacherRepository = mock(TeacherRepository.class);
        ModelMapper mapper = spy(ModelMapper.class);
        TeacherMapper teacherMapper = new TeacherMapper(mapper);
        teacherService = new TeacherServiceImpl(teacherMapper, teacherRepository);
    }

    @Test
    void should_create_one_teacher() {
        var teacherToSave = new Teacher(teacherId, "Test teacher", List.of(Subject.MATH));
        when(teacherRepository.save(any(Teacher.class))).thenReturn(teacherToSave);

        var returnedTeacher = teacherService
                .create(new TeacherRqDto("Test teacher", List.of(Subject.MATH)));

        Assertions.assertEquals(teacherToSave.getId(), returnedTeacher.getId());
        Assertions.assertEquals(teacherToSave.getFio(), returnedTeacher.getFio());
        Assertions.assertEquals(teacherToSave.getSubject(), returnedTeacher.getSubject());
    }

    @Test
    void should_return_teacher_by_id() {
        var teacher = new Teacher(teacherId, "Test teacher", List.of(Subject.MATH));
        when(teacherRepository.findById(teacherId)).thenReturn(Optional.of(teacher));

        var returnedTeacher = teacherService.getById(teacherId);

        Assertions.assertEquals(teacher.getId(), returnedTeacher.getId());
        Assertions.assertEquals(teacher.getFio(), returnedTeacher.getFio());
        Assertions.assertEquals(teacher.getSubject(), returnedTeacher.getSubject());
    }

    @Test
    void should_return_list_of_teachers() {
        var teachers = List.of(
                new Teacher(UUID.randomUUID(), "Teacher1", List.of(Subject.MATH)),
                new Teacher(UUID.randomUUID(), "Teacher2", List.of(Subject.MATH)),
                new Teacher(UUID.randomUUID(), "Teacher3", List.of(Subject.MATH)),
                new Teacher(UUID.randomUUID(), "Teacher4", List.of(Subject.MATH)));
        when(teacherRepository.findAll()).thenReturn(teachers);

        var returnedTeachers = teacherService.getAll();

        Assertions.assertEquals(4, returnedTeachers.size());
        Assertions.assertTrue(returnedTeachers.stream().map(TeacherRsDto::getId).allMatch(Objects::nonNull));
        Assertions.assertTrue(returnedTeachers.stream().map(TeacherRsDto::getFio).allMatch(Objects::nonNull));
    }

    @Test
    void should_update_teachers_data() {
        var teacherBeforeUpdate = new Teacher(teacherId, "Teacher before update", List.of(Subject.MATH));
        var teacherAfterUpdate = new Teacher(teacherId, "Teacher after update", List.of(Subject.MATH, Subject.PHYSICS));
        when(teacherRepository.findById(teacherId)).thenReturn(Optional.of(teacherBeforeUpdate));
        when(teacherRepository.save(any(Teacher.class))).thenReturn(teacherAfterUpdate);

        var returnedTeacher = teacherService.update(teacherId,
                new TeacherRqDto("Teacher after update", List.of(Subject.MATH, Subject.PHYSICS)));

        Assertions.assertEquals(teacherId, returnedTeacher.getId());
        Assertions.assertEquals(List.of(Subject.MATH, Subject.PHYSICS), returnedTeacher.getSubject());
        Assertions.assertEquals("Teacher after update", returnedTeacher.getFio());
    }

    @Test
    void should_delete_teacher_only_once() {
        var teacherForDelete = new Teacher(teacherId, "Teacher", List.of(Subject.MATH));
        when(teacherRepository.findById(teacherId)).thenReturn(Optional.of(teacherForDelete));

        teacherService.delete(teacherId);

        verify(teacherRepository, times(1)).delete(teacherForDelete);
    }

    @Test
    void should_throw_if_teacher_not_found() {
        when(teacherRepository.findById(teacherId)).thenReturn(Optional.empty());

        var exc = Assertions.assertThrows(DataNotFoundException.class,
                () -> teacherService.getById(teacherId));
        Assertions.assertEquals(String.format("Teacher with id = %s not found!", teacherId), exc.getMessage());
    }
}