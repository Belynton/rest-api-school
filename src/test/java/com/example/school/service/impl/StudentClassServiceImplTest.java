package com.example.school.service.impl;

import com.example.school.entity.StudentClass;
import com.example.school.mapper.StudentClassMapper;
import com.example.school.repository.StudentClassRepository;
import com.example.school.rest.dto.StudentClassRqDto;
import com.example.school.rest.dto.StudentClassRsDto;
import com.example.school.rest.extension.DataNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StudentClassServiceImplTest {

    private final UUID studentClassId = UUID.randomUUID();

    private StudentClassRepository studentClassRepository;
    private StudentClassServiceImpl studentClassService;

    @BeforeEach
    void setUp() {
        studentClassRepository = mock(StudentClassRepository.class);
        ModelMapper mapper = spy(ModelMapper.class);
        StudentClassMapper studentClassMapper = new StudentClassMapper(mapper);
        studentClassService = new StudentClassServiceImpl(studentClassMapper, studentClassRepository);
    }

    @Test
    void should_create_one_class() {
        var studentClassToSave = new StudentClass(studentClassId, "Test class", Collections.emptyList());
        when(studentClassRepository.save(any(StudentClass.class))).thenReturn(studentClassToSave);

        var returnedStudentClass = studentClassService.create(new StudentClassRqDto("Test class"));

        Assertions.assertEquals(studentClassToSave.getId(), returnedStudentClass.getId());
        Assertions.assertEquals(studentClassToSave.getName(), returnedStudentClass.getName());
    }

    @Test
    void should_return_class_by_id() {
        var studentClass = new StudentClass(studentClassId, "Test class", Collections.emptyList());
        when(studentClassRepository.findById(studentClassId)).thenReturn(Optional.of(studentClass));

        var returnedStudentClass = studentClassService.getById(studentClassId);

        Assertions.assertEquals(studentClass.getId(), returnedStudentClass.getId());
        Assertions.assertEquals(studentClass.getName(), returnedStudentClass.getName());
    }

    @Test
    void should_return_list_of_classes() {
        var studentClasses = List.of(
                new StudentClass(UUID.randomUUID(), "Class1", Collections.emptyList()),
                new StudentClass(UUID.randomUUID(), "Class2", Collections.emptyList()),
                new StudentClass(UUID.randomUUID(), "Class3", Collections.emptyList()),
                new StudentClass(UUID.randomUUID(), "Class4", Collections.emptyList()));
        when(studentClassRepository.findAll()).thenReturn(studentClasses);

        var returnedStudentClasses = studentClassService.getAll();

        Assertions.assertEquals(4, returnedStudentClasses.size());
        Assertions.assertTrue(returnedStudentClasses.stream().map(StudentClassRsDto::getId).allMatch(Objects::nonNull));
    }

    @Test
    void should_update_class_data() {
        var studentClassBeforeUpdate = new StudentClass(studentClassId, "Class before update", Collections.emptyList());
        var studentClassAfterUpdate = new StudentClass(studentClassId, "Class after update", Collections.emptyList());
        when(studentClassRepository.findById(studentClassId)).thenReturn(Optional.of(studentClassBeforeUpdate));
        when(studentClassRepository.save(any(StudentClass.class))).thenReturn(studentClassAfterUpdate);

        var returnedStudentClass = studentClassService.update(studentClassId,
                new StudentClassRqDto("Class after update"));

        Assertions.assertEquals(studentClassId, returnedStudentClass.getId());
        Assertions.assertEquals("Class after update", returnedStudentClass.getName());
    }

    @Test
    void should_delete_class_only_once() {
        var studentClassForDelete = new StudentClass(studentClassId, "Test class", Collections.emptyList());
        when(studentClassRepository.findById(studentClassId)).thenReturn(Optional.of(studentClassForDelete));

        studentClassService.delete(studentClassId);

        verify(studentClassRepository, times(1)).delete(studentClassForDelete);
    }

    @Test
    void should_throw_if_class_not_found() {
        when(studentClassRepository.findById(studentClassId)).thenReturn(Optional.empty());

        var exc = Assertions.assertThrows(DataNotFoundException.class,
                () -> studentClassService.getById(studentClassId));
        Assertions.assertEquals(String.format("Class with id = %s not found!", studentClassId), exc.getMessage());
    }
}