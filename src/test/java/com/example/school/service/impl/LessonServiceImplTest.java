package com.example.school.service.impl;

import com.example.school.entity.Lesson;
import com.example.school.entity.StudentClass;
import com.example.school.entity.Teacher;
import com.example.school.enums.Subject;
import com.example.school.mapper.LessonMapper;
import com.example.school.repository.LessonRepository;
import com.example.school.repository.StudentClassRepository;
import com.example.school.repository.TeacherRepository;
import com.example.school.rest.dto.LessonRqDto;
import com.example.school.rest.dto.LessonRsDto;
import com.example.school.rest.extension.DataNotFoundException;
import com.example.school.rest.extension.NotSupportedSubjectException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LessonServiceImplTest {

    private final UUID lessonId = UUID.randomUUID();
    private final UUID teacherId = UUID.randomUUID();
    private final UUID classId = UUID.randomUUID();
    private final LocalDateTime dateTime = LocalDateTime.now();
    private final Subject subject = Subject.MATH;

    private LessonRepository lessonRepository;
    private TeacherRepository teacherRepository;
    private StudentClassRepository studentClassRepository;
    private LessonServiceImpl lessonService;

    @BeforeEach
    void setUp() {
        studentClassRepository = mock(StudentClassRepository.class);
        lessonRepository = mock(LessonRepository.class);
        teacherRepository = mock(TeacherRepository.class);
        ModelMapper mapper = spy(ModelMapper.class);
        LessonMapper lessonMapper = new LessonMapper(mapper);
        lessonService = new LessonServiceImpl(lessonMapper,
                lessonRepository,
                teacherRepository,
                studentClassRepository);
    }

    @Test
    void should_create_one_lesson() {
        var teacher = new Teacher(teacherId, "Test teacher", List.of(subject));
        var studentClass = new StudentClass(classId, "Test class", Collections.emptyList());
        var lessonToSave = new Lesson(lessonId, dateTime, subject, teacher, studentClass);

        when(studentClassRepository.findById(classId)).thenReturn(Optional.of(studentClass));
        when(teacherRepository.findById(teacherId)).thenReturn(Optional.of(teacher));
        when(lessonRepository.save(any(Lesson.class))).thenReturn(lessonToSave);

        var returnedLesson = lessonService
                .create(new LessonRqDto(dateTime, subject, teacherId, classId));

        Assertions.assertEquals(lessonToSave.getId(), returnedLesson.getId());
        Assertions.assertEquals(lessonToSave.getSubject(), returnedLesson.getSubject());
        Assertions.assertEquals(lessonToSave.getStartDateTime(), returnedLesson.getStartDateTime());
    }

    @Test
    void should_throw_if_teacher_not_supports_subject() {
        var unsupportedSubject = Subject.BIOLOGY;
        var teacher = new Teacher(teacherId, "Test teacher", List.of(unsupportedSubject));

        when(teacherRepository.findById(teacherId)).thenReturn(Optional.of(teacher));

        var exc = Assertions.assertThrows(NotSupportedSubjectException.class,
                () -> lessonService.create(new LessonRqDto(dateTime, subject, teacherId, classId)));
        Assertions.assertEquals(String.format("Subject %s can't be applied to this teacher!", subject), exc.getMessage());
    }

    @Test
    void should_return_lesson_by_id() {
        var lesson = new Lesson(lessonId, dateTime, Subject.MATH, new Teacher(), new StudentClass());
        when(lessonRepository.findById(lessonId)).thenReturn(Optional.of(lesson));

        var returnedLesson = lessonService.getById(lessonId);

        Assertions.assertEquals(lesson.getId(), returnedLesson.getId());
        Assertions.assertEquals(lesson.getSubject(), returnedLesson.getSubject());
        Assertions.assertEquals(lesson.getStartDateTime(), returnedLesson.getStartDateTime());
    }

    @Test
    void should_return_list_of_all_lessons() {
        var lessons = List.of(
                new Lesson(UUID.randomUUID(), LocalDateTime.now(), Subject.MATH, new Teacher(), new StudentClass()),
                new Lesson(UUID.randomUUID(), LocalDateTime.now(), Subject.MATH, new Teacher(), new StudentClass()),
                new Lesson(UUID.randomUUID(), LocalDateTime.now(), Subject.MATH, new Teacher(), new StudentClass()),
                new Lesson(UUID.randomUUID(), LocalDateTime.now(), Subject.MATH, new Teacher(), new StudentClass()));
        when(lessonRepository.findAll()).thenReturn(lessons);

        var returnedLessons = lessonService.getAll();

        Assertions.assertEquals(4, returnedLessons.size());
        Assertions.assertTrue(returnedLessons.stream().map(LessonRsDto::getId).allMatch(Objects::nonNull));
        Assertions.assertTrue(returnedLessons.stream().map(LessonRsDto::getSubject).allMatch(Objects::nonNull));
        Assertions.assertTrue(returnedLessons.stream().map(LessonRsDto::getStartDateTime).allMatch(Objects::nonNull));
    }

    @Test
    void should_return_list_of_lessons_by_date() {
        var lessons = List.of(
                new Lesson(UUID.randomUUID(), dateTime, subject, new Teacher(), new StudentClass()),
                new Lesson(UUID.randomUUID(), dateTime, subject, new Teacher(), new StudentClass()),
                new Lesson(UUID.randomUUID(), dateTime, subject, new Teacher(), new StudentClass()),
                new Lesson(UUID.randomUUID(), dateTime, subject, new Teacher(), new StudentClass()));
        when(lessonRepository.getLessonsByStartDateTime(dateTime.toLocalDate())).thenReturn(lessons);

        var returnedLessons = lessonService.getAllByDate(dateTime.toLocalDate());

        Assertions.assertEquals(4, returnedLessons.size());
        Assertions.assertTrue(returnedLessons.stream().map(LessonRsDto::getId).allMatch(Objects::nonNull));
        Assertions.assertTrue(returnedLessons.stream().map(LessonRsDto::getSubject).allMatch(Objects::nonNull));
        Assertions.assertTrue(returnedLessons.stream().map(LessonRsDto::getStartDateTime)
                .allMatch(dt -> dt.toLocalDate().isEqual(ChronoLocalDate.from(dateTime))));
    }

    @Test
    void should_return_list_of_lessons_by_subject() {
        var lessons = List.of(
                new Lesson(UUID.randomUUID(), dateTime, subject, new Teacher(), new StudentClass()),
                new Lesson(UUID.randomUUID(), dateTime, subject, new Teacher(), new StudentClass()),
                new Lesson(UUID.randomUUID(), dateTime, subject, new Teacher(), new StudentClass()),
                new Lesson(UUID.randomUUID(), dateTime, subject, new Teacher(), new StudentClass()));
        when(lessonRepository.getLessonsBySubject(subject)).thenReturn(lessons);

        var returnedLessons = lessonService.getAllBySubject(subject);

        Assertions.assertEquals(4, returnedLessons.size());
        Assertions.assertTrue(returnedLessons.stream().map(LessonRsDto::getId).allMatch(Objects::nonNull));
        Assertions.assertTrue(returnedLessons.stream().map(LessonRsDto::getStartDateTime).allMatch(Objects::nonNull));
        Assertions.assertTrue(returnedLessons.stream().map(LessonRsDto::getSubject)
                .allMatch(subj -> subj.equals(subject)));
    }

    @Test
    void should_return_list_of_lessons_by_class() {
        var studentClass = new StudentClass(classId, "Class 1", Collections.emptyList());
        var lessons = List.of(
                new Lesson(UUID.randomUUID(), dateTime, subject, new Teacher(), studentClass),
                new Lesson(UUID.randomUUID(), dateTime, subject, new Teacher(), studentClass),
                new Lesson(UUID.randomUUID(), dateTime, subject, new Teacher(), studentClass),
                new Lesson(UUID.randomUUID(), dateTime, subject, new Teacher(), studentClass));
        when(lessonRepository.getLessonsByStudentClass_Id(classId)).thenReturn(lessons);

        var returnedLessons = lessonService.getAllByStudentClass(classId);

        Assertions.assertEquals(4, returnedLessons.size());
        Assertions.assertTrue(returnedLessons.stream().map(LessonRsDto::getId).allMatch(Objects::nonNull));
        Assertions.assertTrue(returnedLessons.stream().map(LessonRsDto::getSubject).allMatch(Objects::nonNull));
        Assertions.assertTrue(returnedLessons.stream().map(LessonRsDto::getStartDateTime).allMatch(Objects::nonNull));
        Assertions.assertTrue(returnedLessons.stream().map(LessonRsDto::getStudentClass)
                .allMatch(classRsDto -> classRsDto.getId().equals(classId)));
    }

    @Test
    void should_update_lessons_data() {
        var updatedDateTime = LocalDateTime.now();
        var teacher = new Teacher(teacherId, "Test teacher", List.of(subject));
        var studentClass = new StudentClass(classId, "Test class", Collections.emptyList());
        var lessonBeforeUpdate = new Lesson(lessonId, dateTime, subject, teacher, studentClass);
        var lessonAfterUpdate = new Lesson(lessonId, updatedDateTime, subject, teacher, studentClass);

        when(studentClassRepository.findById(classId)).thenReturn(Optional.of(studentClass));
        when(teacherRepository.findById(teacherId)).thenReturn(Optional.of(teacher));
        when(lessonRepository.findById(lessonId)).thenReturn(Optional.of(lessonBeforeUpdate));
        when(lessonRepository.save(any(Lesson.class))).thenReturn(lessonAfterUpdate);

        var returnedLesson = lessonService
                .update(lessonId, new LessonRqDto(updatedDateTime, subject, teacherId, classId));

        Assertions.assertEquals(lessonId, returnedLesson.getId());
        Assertions.assertEquals(subject, returnedLesson.getSubject());
        Assertions.assertEquals(updatedDateTime, returnedLesson.getStartDateTime());
    }

    @Test
    void should_delete_lesson_only_once() {
        var lessonForDelete = new Lesson(lessonId, LocalDateTime.now(), Subject.MATH, new Teacher(), new StudentClass());
        when(lessonRepository.findById(lessonId)).thenReturn(Optional.of(lessonForDelete));

        lessonService.delete(lessonId);

        verify(lessonRepository, times(1)).delete(lessonForDelete);
    }

    @Test
    void should_throw_if_teacher_not_found() {
        var lessonRqDto = new LessonRqDto(LocalDateTime.now(), Subject.MATH, teacherId, classId);
        when(teacherRepository.findById(teacherId)).thenReturn(Optional.empty());

        var exc = Assertions.assertThrows(DataNotFoundException.class,
                () -> lessonService.create(lessonRqDto));
        Assertions.assertEquals(String.format("Teacher with id = %s not found!", teacherId), exc.getMessage());
    }

    @Test
    void should_throw_if_studentClass_not_found() {
        var lessonRqDto = new LessonRqDto(LocalDateTime.now(), subject, teacherId, classId);
        var teacher = new Teacher(teacherId, "Test teacher", List.of(subject));
        when(teacherRepository.findById(teacherId)).thenReturn(Optional.of(teacher));
        when(studentClassRepository.findById(classId)).thenReturn(Optional.empty());

        var exc = Assertions.assertThrows(DataNotFoundException.class,
                () -> lessonService.create(lessonRqDto));
        Assertions.assertEquals(String.format("Class with id = %s not found!", classId), exc.getMessage());
    }

    @Test
    void should_throw_if_lesson_not_found() {
        when(lessonRepository.findById(lessonId)).thenReturn(Optional.empty());

        var exc = Assertions.assertThrows(DataNotFoundException.class,
                () -> lessonService.getById(lessonId));
        Assertions.assertEquals(String.format("Lesson with id = %s not found!", lessonId), exc.getMessage());
    }

}