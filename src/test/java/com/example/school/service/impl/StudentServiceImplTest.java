package com.example.school.service.impl;

import com.example.school.entity.Student;
import com.example.school.entity.StudentClass;
import com.example.school.mapper.StudentMapper;
import com.example.school.repository.StudentClassRepository;
import com.example.school.repository.StudentRepository;
import com.example.school.rest.dto.StudentRqDto;
import com.example.school.rest.dto.StudentRsDto;
import com.example.school.rest.extension.DataNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StudentServiceImplTest {

    private final UUID studentId = UUID.randomUUID();
    private final UUID studentClassId = UUID.randomUUID();

    private StudentRepository studentRepository;
    private StudentClassRepository studentClassRepository;
    private StudentServiceImpl studentService;

    @BeforeEach
    void setUp() {
        studentClassRepository = mock(StudentClassRepository.class);
        studentRepository = mock(StudentRepository.class);
        ModelMapper mapper = spy(ModelMapper.class);
        StudentMapper studentMapper = new StudentMapper(mapper);
        studentService = new StudentServiceImpl(studentMapper, studentRepository, studentClassRepository);
    }

    @Test
    void should_create_one_student() {
        var studentClass = new StudentClass(studentClassId, "Test class", Collections.emptyList());
        var studentToSave = new Student(studentId, "Test student", studentClass);
        when(studentClassRepository.findById(studentClassId)).thenReturn(Optional.of(studentClass));
        when(studentRepository.save(any(Student.class))).thenReturn(studentToSave);

        var returnedStudent = studentService.create(new StudentRqDto("Test student", studentClassId));

        Assertions.assertEquals(studentToSave.getId(), returnedStudent.getId());
        Assertions.assertEquals(studentToSave.getFio(), returnedStudent.getFio());
    }

    @Test
    void should_return_student_by_id() {
        var studentToFind = new Student(studentId, "Test student", new StudentClass());
        when(studentRepository.findById(studentId)).thenReturn(Optional.of(studentToFind));

        var returnedStudent = studentService.getById(studentId);

        Assertions.assertEquals(studentToFind.getId(), returnedStudent.getId());
        Assertions.assertEquals(studentToFind.getFio(), returnedStudent.getFio());
    }

    @Test
    void should_return_list_of_students() {
        var students = List.of(
                new Student(UUID.randomUUID(), "Student1", new StudentClass()),
                new Student(UUID.randomUUID(), "Student2", new StudentClass()),
                new Student(UUID.randomUUID(), "Student3", new StudentClass()),
                new Student(UUID.randomUUID(), "Student4", new StudentClass()));
        when(studentRepository.findAll()).thenReturn(students);

        var returnedClasses = studentService.getAll();

        Assertions.assertEquals(4, returnedClasses.size());
        Assertions.assertTrue(returnedClasses.stream().map(StudentRsDto::getId).allMatch(Objects::nonNull));
    }

    @Test
    void should_update_students_data() {
        var studentClass = new StudentClass(studentClassId, "Test class", Collections.emptyList());
        var studentBeforeUpdate = new Student(studentId, "Student before update", studentClass);
        var studentAfterUpdate = new Student(studentId, "Student before update", studentClass);
        when(studentClassRepository.findById(studentClassId)).thenReturn(Optional.of(studentClass));
        when(studentRepository.findById(studentId)).thenReturn(Optional.of(studentBeforeUpdate));
        when(studentRepository.save(any(Student.class))).thenReturn(studentAfterUpdate);

        var returnedStudent = studentService
                .update(studentId, new StudentRqDto("Student after update", studentClassId));

        Assertions.assertEquals(studentAfterUpdate.getId(), returnedStudent.getId());
        Assertions.assertEquals(studentAfterUpdate.getFio(), returnedStudent.getFio());
    }

    @Test
    void should_delete_student_only_once() {
        var studentForDelete = new Student(studentId, "Student before update",
                new StudentClass(UUID.randomUUID(), "Test class", Collections.emptyList()));
        when(studentRepository.findById(studentId)).thenReturn(Optional.of(studentForDelete));

        studentService.delete(studentId);

        verify(studentRepository, times(1)).delete(studentForDelete);
    }

    @Test
    void should_throw_if_student_not_found() {
        when(studentRepository.findById(studentId)).thenReturn(Optional.empty());

        var exc = Assertions.assertThrows(DataNotFoundException.class,
                () -> studentService.getById(studentId));
        Assertions.assertEquals(String.format("Student with id = %s not found!", studentId), exc.getMessage());
    }

    @Test
    void should_throw_if_class_not_found() {
        when(studentClassRepository.findById(studentClassId)).thenReturn(Optional.empty());

        var exc = Assertions.assertThrows(DataNotFoundException.class,
                () -> studentService.create(new StudentRqDto("Test student", studentClassId)));
        Assertions.assertEquals(String.format("Class with id = %s not found!", studentClassId), exc.getMessage());
    }
}